import java.util.LinkedList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion implements Cloneable {

    private double r, i, j, k;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        r = a;
        i = b;
        j = c;
        k = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    double getRpart() {
        return r;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    double getIpart() {
        return i;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    double getJpart() {
        return j;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    double getKpart() {
        return k;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return r string form of this quaternion:
     * "r+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(r);
        if (i < 0) {
            stringBuilder.append(i).append("i");
        } else {
            stringBuilder.append("+").append(i).append("i");
        }
        if (j < 0) {
            stringBuilder.append(j).append("j");
        } else {
            stringBuilder.append("+").append(j).append("j");
        }
        if (k < 0) {
            stringBuilder.append(k).append("k");
        } else {
            stringBuilder.append("+").append(k).append("k");
        }

        return stringBuilder.toString();

    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return r quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  r quaternion (defined by the <code>toString</code> method)
     */
    static Quaternion valueOf(String s) {
        String s1 = s.replaceAll("\\P{L}+", "");
        for (int i = 0; i < s1.length(); i++) {
            for (int j = i + 1; j < s1.length(); j++) {
                if (s1.charAt(i) == s1.charAt(j)) {
                    throw new RuntimeException("Error [" + s + "] contains duplicates..");
                }
            }
        }
        LinkedList<String> list = new LinkedList<>();

        Pattern pattern = Pattern.compile("(-?(\\d)+(\\.)?(\\d)*)");
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()){
            list.add(matcher.group());
        }
        if(list.size() == 4){
            double r = Double.parseDouble(list.get(0));
            double i = Double.parseDouble(list.get(1));
            double j = Double.parseDouble(list.get(2));
            double k = Double.parseDouble(list.get(3));

            System.out.println(list);
            return new Quaternion(r, i, j, k);
        } else
            throw new IllegalArgumentException("String " + s + " is not an quaternion.");


    }


    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(r, i, j, k);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    boolean isZero() {
        return equals(new Quaternion(0., 0., 0., 0.));
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(r+bi+cj+dk) = r-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    Quaternion conjugate() {
        return new Quaternion(r, -i, -j, -k);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(r+bi+cj+dk) = -r-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    Quaternion opposite() {
        return new Quaternion(-r, -i, -j, -k);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    Quaternion plus(Quaternion q) {
        return new Quaternion(r + q.getRpart(), i + q.getIpart(), j + q.getJpart(), k + q.getKpart());
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    Quaternion times(Quaternion q) {
        return new Quaternion(
                r * q.r - i * q.i - j * q.j - k * q.k,
                r * q.i + q.r * i + j * q.k - k * q.j,
                r * q.j - i * q.k + j * q.r + k * q.i,
                r * q.k + i * q.j - j * q.i + k * q.r
        );
    }

    /**
     * Multiplication by r coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    Quaternion times(double r) {
        return new Quaternion(this.r * r, i * r, j * r, k * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(r+bi+cj+dk) = r/(r*r+i*i+j*j+k*k) +
     * ((-i)/(r*r+i*i+j*j+k*k))i + ((-j)/(r*r+i*i+j*j+k*k))j + ((-k)/(r*r+i*i+j*j+k*k))k
     *
     * @return quaternion <code>1/this</code>
     */
    Quaternion inverse() {
        if (isZero())
            throw new IllegalArgumentException();
        double inverse = (r * r + i * i + j * j + k * k);
        return new Quaternion(r / inverse, -i / inverse, -j / inverse, -k / inverse);
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    Quaternion minus(Quaternion q) {
        return new Quaternion(r - q.getRpart(), i - q.getIpart(), j - q.getJpart(), k - q.getKpart());
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    Quaternion divideByRight(Quaternion q) {
        if (isZero())
            throw new IllegalArgumentException();
        return this.times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    Quaternion divideByLeft(Quaternion q) {
        if (isZero())
            throw new IllegalArgumentException();
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object qo) {
        Quaternion quaternion = (Quaternion) qo;
        return Math.abs(quaternion.r - r) < 0.00001 && Math.abs(quaternion.i - i) < 0.00001 &&
                Math.abs(quaternion.j - j) < 0.00001 && Math.abs(quaternion.k - k) < 0.00001;

    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    Quaternion dotMult(Quaternion q) {
        return this.times(q.conjugate()).plus(q.times(this.conjugate())).times(0.5);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.r, this.i, this.j, this.k);
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(r+bi+cj+dk) = Math.sqrt(r*r+i*i+j*j+k*k)
     *
     * @return norm of <code>this</code> (norm is r real number)
     */
    double norm() {
        return Math.sqrt((r * r) + (i * i) + (j * j) + (k * k));
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
        Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
        if (arg.length > 0)
            arv1 = valueOf (arg[0]);
        System.out.println ("first: " + arv1.toString());
        System.out.println ("real: " + arv1.getRpart());
        System.out.println ("imagi: " + arv1.getIpart());
        System.out.println ("imagj: " + arv1.getJpart());
        System.out.println ("imagk: " + arv1.getKpart());
        System.out.println ("isZero: " + arv1.isZero());
        System.out.println ("conjugate: " + arv1.conjugate());
        System.out.println ("opposite: " + arv1.opposite());
        System.out.println ("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion)arv1.clone();
        } catch (CloneNotSupportedException e) {};
        System.out.println ("clone equals to original: " + res.equals (arv1));
        System.out.println ("clone is not the same object: " + (res!=arv1));
        System.out.println ("hashCode: " + res.hashCode());
        res = valueOf (arv1.toString());
        System.out.println ("string conversion equals to original: "
                + res.equals (arv1));
        Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf (arg[1]);
        System.out.println ("second: " + arv2.toString());
        System.out.println ("hashCode: " + arv2.hashCode());
        System.out.println ("equals: " + arv1.equals (arv2));
        res = arv1.plus (arv2);
        System.out.println ("plus: " + res);
        System.out.println ("times: " + arv1.times (arv2));
        System.out.println ("minus: " + arv1.minus (arv2));
        double mm = arv1.norm();
        System.out.println ("norm: " + mm);
        System.out.println ("inverse: " + arv1.inverse());
        System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
        System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
        System.out.println ("dotMult: " + arv1.dotMult (arv2));
    }
}

/*
Kasutatud materjalid:
https://stackoverflow.com/questions/11597386/objects-hash-vs-objects-hashcode-clarification-needed
https://introcs.cs.princeton.edu/java/32class/Quaternion.java.html
https://www.javamex.com/tutorials/regular_expressions/

 */

// end of file

